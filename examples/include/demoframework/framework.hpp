#ifndef _demoframework_framework_hpp_
#define _demoframework_framework_hpp_

#include <SDL.h>
#include <rendercore/rendering-api.hpp>

namespace demo {

struct SDLWindowWrapper {
	SDLWindowWrapper();
	~SDLWindowWrapper();
	SDL_Window* ptr;
};

class Framework
{
public:
	Framework();
	~Framework();

	Framework(const Framework&) = delete;
	Framework& operator=(const Framework&) = delete;

	void run();

private: // demo functions
	const char* getDemoName() const;
	void demoInitialize();
	void demoTerminate();
	void demoTick(float deltaTime);
	bool demoProcessSDLEvent(SDL_Event evt);

private:
	void computeWindowSize(int& width, int& height, bool& fullscreen);

	int m_wndWidth;
	int m_wndHeight;
	SDLWindowWrapper m_window;
	std::unique_ptr<rendercore::RenderingAPI> m_renderingApi;
	rendercore::Swapchain* m_swapchain;
};

} // namespace demo

#endif // _demoframework_framework_hpp_
