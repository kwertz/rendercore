#ifndef _demoframework_filehelper_hpp_
#define _demoframework_filehelper_hpp_

#include <vector>
#include <string>
#include <cstdint>

namespace demo {

	std::string readWholeFile(const std::string& filename);
	std::vector<uint8_t> readWholeFileBinary(const std::string& filename);

} // namespace demo

#endif // _demoframework_filehelper_hpp_
