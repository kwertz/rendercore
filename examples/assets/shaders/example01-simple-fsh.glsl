#version 330

out vec4 fragColor;

smooth in vec2 vfTexcoords;

layout(std140) uniform FragmentShaderUniforms
{
	vec4 someColor;
};

void main()
{
	float dist = 1.0f - length(vec2(0.5f, 0.5f) - vfTexcoords);
	fragColor = vec4(vec3(dist), 1) * someColor;
}
