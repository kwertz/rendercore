#version 330

in vec4 position;
in vec2 texcoords;

smooth out vec2 vfTexcoords;

void main()
{
	gl_Position = position;
	vfTexcoords = texcoords;
}
