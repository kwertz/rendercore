#version 330

out vec4 fragColor;

smooth in vec2 vfTexcoords;

uniform sampler2D baseMap;

void main()
{
	vec4 accumulator = vec4(0.0f);
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(-1, -1));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(0, -1));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(1, -1));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(-1, 0));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(0, 0));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(1, 0));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(-1, 1));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(0, 1));
	accumulator += textureOffset(baseMap, vfTexcoords, ivec2(1, 1));
	fragColor = vec4(1.0f, 1.0f, 1.0f, 0.0f) - (accumulator / 9.0f);
}
