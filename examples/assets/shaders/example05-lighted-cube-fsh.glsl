#version 430

out vec4 outFragColor;

layout(std140) uniform FragmentShaderUniforms
{
	vec3 diffuseLightColor;
	vec3 specularLightColor;
};

uniform sampler2D baseMap;
uniform sampler2D normalMap;
uniform sampler2D specMap;

smooth in vec2 vfTexcoords;
smooth in vec3 vfLightDir;
smooth in vec3 vfViewDir;
smooth in vec3 vfSurfToLightWorld;

void main()
{
	vec3 lightDir = normalize(vfLightDir);
	vec3 viewDir = normalize(vfViewDir);
	
	vec3 normal = normalize((texture(normalMap, vfTexcoords).rgb * 2.0f) - 1.0f); // map [0, 1] to [-1, 1]
	float NdotL = dot(normal, lightDir);
	float diffuse = clamp(NdotL, 0.0f, 1.0f);
	
	vec3 halfway = normalize(lightDir + viewDir);
	float specular = clamp(dot(normal, halfway), 0.0f, 1.0f);
	
	float sqDist = dot(vfSurfToLightWorld, vfSurfToLightWorld);
	float distAtten = 1.0f / (1.0f + sqDist);
	//float distAtten = 1.0f;
	
	//float NdotV = dot(normal, viewDir);
	
	vec4 baseColor = texture(baseMap, vfTexcoords);
	vec3 totalAmbient = vec3(0.05f) * baseColor.rgb;
	//vec3 totalAmbient = vec3(0.0f);
	vec3 totalDiffuse = vec3(diffuse) * diffuseLightColor * baseColor.rgb * distAtten;
	vec3 totalSpecular = vec3(pow(specular, 40.0f)) * step(0.0f, NdotL) * specularLightColor * texture(specMap, vfTexcoords).r * distAtten;
	//totalSpecular *= diffuse; // attenuate by N.L?
	
	outFragColor = vec4(totalAmbient + totalDiffuse + totalSpecular, 1.0f);
}
