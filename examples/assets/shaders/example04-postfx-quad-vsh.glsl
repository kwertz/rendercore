#version 330

in vec4 position;

smooth out vec2 vfTexcoords;

void main()
{
	gl_Position = position;
	vfTexcoords = position.xy * 0.5f + 0.5f;
}
