#version 430

in vec4 position;
in vec2 texcoords;
in vec3 normal;
in vec3 tangent;
in vec3 bitangent;

layout(std140) uniform VertexShaderUniforms
{
	mat4 mvpMat;
	mat4 mvMat;
	mat4 modelMat;
	mat4 viewMat;
	vec3 lightPos;
	vec3 camPos;
};

smooth out vec2 vfTexcoords;
smooth out vec3 vfLightDir;
smooth out vec3 vfViewDir;
smooth out vec3 vfSurfToLightWorld;

void main()
{
	vec4 actualPosition = position;
	if (gl_InstanceID > 0) {
		// pow(-1, y) isn't supported, even for integer y
		float s = 1.0f - 2.0f * mod(float(gl_InstanceID) - 1.0f, 2);
		float f = floor((1 + gl_InstanceID) * 0.5f) * 2.1f;
		actualPosition.x += s * f;
	}

	gl_Position = mvpMat * actualPosition;
	vfTexcoords = texcoords;
	
	vec4 objPos = mvMat * actualPosition;
	
	vec3 lightDir = (viewMat * vec4(lightPos, 1.0f)).xyz - objPos.xyz;
	vec3 viewDir = (viewMat * vec4(camPos, 1.0f)).xyz - objPos.xyz;
	
	mat3 normalMat = transpose(inverse(mat3(mvMat)));
	
	vec3 normal_ = normalMat * normal;
	vec3 tangent_ = normalMat * tangent;
	vec3 bitangent_ = normalMat * bitangent;
	
	mat3 tbnMat = transpose(mat3(tangent_, bitangent_, normal_));
	
	vfLightDir = tbnMat * lightDir;
	vfViewDir = tbnMat * viewDir;
	
	vfSurfToLightWorld = lightPos - (modelMat * actualPosition).xyz;
}
