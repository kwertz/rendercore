#version 330

out vec4 fragColor;

smooth in vec2 vfTexcoords;

layout(std140) uniform FragmentShaderUniforms
{
	vec4 someColor;
};

uniform sampler2D baseMap;

void main()
{
	fragColor = texture(baseMap, vfTexcoords);
}
