#version 430

out vec4 outFragColor;

layout(std140) uniform FragmentShaderUniforms
{
	vec3 diffuseLightColor;
	vec3 specularLightColor;
};

uniform sampler2D baseMap;
uniform sampler2D normalMap;
uniform sampler2D specMap;

smooth in vec2 vfTexcoords;
smooth in vec3 vfLightDir1;
smooth in vec3 vfLightDir2;
smooth in vec3 vfViewDir;
smooth in vec3 vfSurfToLightWorld1;
smooth in vec3 vfSurfToLightWorld2;

void main()
{
	vec3 lightDir1 = normalize(vfLightDir1);
	vec3 lightDir2 = normalize(vfLightDir2);
	vec3 viewDir = normalize(vfViewDir);
	
	vec3 normal = normalize((texture(normalMap, vfTexcoords).rgb * 2.0f) - 1.0f); // map [0, 1] to [-1, 1]
	float NdotL1 = dot(normal, lightDir1);
	float NdotL2 = dot(normal, lightDir2);
	float diffuse1 = clamp(NdotL1, 0.0f, 1.0f);
	float diffuse2 = clamp(NdotL2, 0.0f, 1.0f);
	
	vec3 halfway1 = normalize(lightDir1 + viewDir);
	vec3 halfway2 = normalize(lightDir2 + viewDir);
	float specular1 = clamp(dot(normal, halfway1), 0.0f, 1.0f);
	float specular2 = clamp(dot(normal, halfway2), 0.0f, 1.0f);
	
	float sqDist1 = dot(vfSurfToLightWorld1, vfSurfToLightWorld1);
	float sqDist2 = dot(vfSurfToLightWorld2, vfSurfToLightWorld2);
	float distAtten1 = 1.0f / (1.0f + sqDist1);
	float distAtten2 = 1.0f / (1.0f + sqDist2);
	
	vec4 baseColor = texture(baseMap, vfTexcoords);
	vec3 totalAmbient = vec3(0.05f) * baseColor.rgb;
	vec3 totalDiffuse1 = vec3(diffuse1) * diffuseLightColor * baseColor.rgb * distAtten1;
	vec3 totalDiffuse2 = vec3(diffuse2) * diffuseLightColor * baseColor.rgb * distAtten2;
	vec3 totalSpecular1 = vec3(pow(specular1, 40.0f)) * step(0.0f, NdotL1) * specularLightColor * texture(specMap, vfTexcoords).r * distAtten1;
	vec3 totalSpecular2 = vec3(pow(specular2, 40.0f)) * step(0.0f, NdotL2) * specularLightColor * texture(specMap, vfTexcoords).r * distAtten2;
	
	outFragColor = vec4(totalAmbient + totalDiffuse1 + totalDiffuse2 + totalSpecular1 + totalSpecular2, 1.0f);
}
