#version 330

out vec4 fragColor;

smooth in vec2 vfTexcoords;

uniform sampler2D baseMap;

float random(vec3 seed, int i)
{
	vec4 seed4 = vec4(seed, i);
	float dot_product = dot(seed4, vec4(12.9898f, 78.233f, 45.164f, 94.673f));
	return fract(sin(dot_product) * 43758.5453f);
}

vec3 dither(vec2 co)
{
	return vec3(
		random(vec3(co, 0.0f), 0),
		random(vec3(co, 0.0f), 1),
		random(vec3(co, 0.0f), 2)
	);
}

const float ditherCoeff = 5.077051900661759e-6; // (1/255)^2.2

void main()
{
	// uniform noise dithering
	// effectively removes banding artifacts
	fragColor = texture(baseMap, vfTexcoords) + vec4((dither(vfTexcoords) * ditherCoeff), 0.0f);
}
