#version 330

in vec4 position;
in vec2 texcoords;
in vec3 normal;

smooth out vec2 vfTexcoords;
smooth out vec3 vfNormal;

layout(std140) uniform VertexShaderUniforms
{
	mat4 mvpMat;
};

void main()
{
	gl_Position = mvpMat * position;
	vfTexcoords = texcoords;
	vfNormal = normal;
}
