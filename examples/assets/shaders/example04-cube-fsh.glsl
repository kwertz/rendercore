#version 330

out vec4 fragColor;
smooth in vec2 vfTexcoords;
smooth in vec3 vfNormal;

layout(std140) uniform FragmentShaderUniforms
{
	vec4 someColor;
};

uniform sampler2D baseMap;
uniform sampler2D decalMap;

void main()
{
	vec4 baseColor = texture(baseMap, vfTexcoords);
	vec4 decalColor = texture(decalMap, vfTexcoords);
	fragColor = mix(baseColor, decalColor, decalColor.a);
}
