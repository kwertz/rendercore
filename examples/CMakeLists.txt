cmake_minimum_required(VERSION 3.1)
project("rendercore_examples")

set(GL3W_HEADER_FILES
	"../library/thirdparty/gl3w/include/GL/gl3w.h"
	"../library/thirdparty/gl3w/include/GL/glcorearb.h")
set(GL3W_SOURCE_FILES
	"../library/thirdparty/gl3w/src/gl3w.c")

include_directories("../library/thirdparty/gl3w/include")
add_library(gl3w STATIC ${GL3W_SOURCE_FILES} ${GL3W_HEADER_FILES})

add_subdirectory("thirdparty/sdl2")
include_directories("thirdparty/sdl2/include" "thirdparty/eigen" "${CMAKE_BINARY_DIR}/thirdparty/sdl2/include")

configure_file(
	"src/example-common/config.hpp.in"
	"${PROJECT_BINARY_DIR}/src/example-common/config.hpp"
)

include_directories("include" "../library/include" "${PROJECT_BINARY_DIR}/src/example-common")

set(DEMOFRAMEWORK_HEADER_FILES
	"include/demoframework/filehelper.hpp"
	"include/demoframework/framework.hpp")
set(DEMOFRAMEWORK_SOURCE_FILES
	"src/demoframework/filehelper.cpp"
	"src/demoframework/framework.cpp")

add_library(demoframework STATIC ${DEMOFRAMEWORK_SOURCE_FILES} ${DEMOFRAMEWORK_HEADER_FILES})
set_property(TARGET demoframework PROPERTY CXX_STANDARD 11)
set_property(TARGET demoframework PROPERTY CXX_STANDARD_REQUIRED ON)
target_link_libraries(demoframework PUBLIC rendercore gl3w SDL2main SDL2)

set(EXAMPLE01_BASICS_HEADER_FILES
	"${PROJECT_BINARY_DIR}/src/example-common/config.hpp")
set(EXAMPLE01_BASICS_SOURCE_FILES
	"src/example01-basics/example.cpp")

add_executable(example01 WIN32 ${EXAMPLE01_BASICS_SOURCE_FILES} ${EXAMPLE01_BASICS_HEADER_FILES})
set_property(TARGET example01 PROPERTY CXX_STANDARD 11)
set_property(TARGET example01 PROPERTY CXX_STANDARD_REQUIRED ON)
target_link_libraries(example01 demoframework)

set(EXAMPLE02_HEADER_FILES
	"${PROJECT_BINARY_DIR}/src/example-common/config.hpp"
	"src/example-common/stb_image.h")
set(EXAMPLE02_SOURCE_FILES
	"src/example02-textures/example.cpp")

add_executable(example02 WIN32 ${EXAMPLE02_HEADER_FILES} ${EXAMPLE02_SOURCE_FILES})
set_property(TARGET example02 PROPERTY CXX_STANDARD 11)
set_property(TARGET example02 PROPERTY CXX_STANDARD_REQUIRED ON)
target_link_libraries(example02 demoframework)

set(EXAMPLE03_HEADER_FILES
	"${PROJECT_BINARY_DIR}/src/example-common/config.hpp"
	"src/example-common/stb_image.h"
	"src/example-common/stb_image_resize.h"
	"src/example-common/cube.hpp")
set(EXAMPLE03_SOURCE_FILES
	"src/example03-3dmesh/example.cpp")

add_executable(example03 WIN32 ${EXAMPLE03_HEADER_FILES} ${EXAMPLE03_SOURCE_FILES})
set_property(TARGET example03 PROPERTY CXX_STANDARD 11)
set_property(TARGET example03 PROPERTY CXX_STANDARD_REQUIRED ON)
target_link_libraries(example03 demoframework)

set(EXAMPLE04_HEADER_FILES
	"${PROJECT_BINARY_DIR}/src/example-common/config.hpp"
	"src/example-common/stb_image.h"
	"src/example-common/stb_image_resize.h"
	"src/example-common/cube.hpp")
set(EXAMPLE04_SOURCE_FILES
	"src/example04-postfx/example.cpp")

add_executable(example04 WIN32 ${EXAMPLE04_HEADER_FILES} ${EXAMPLE04_SOURCE_FILES})
set_property(TARGET example04 PROPERTY CXX_STANDARD 11)
set_property(TARGET example04 PROPERTY CXX_STANDARD_REQUIRED ON)
target_link_libraries(example04 demoframework)

set(EXAMPLE05_HEADER_FILES
	"${PROJECT_BINARY_DIR}/src/example-common/config.hpp"
	"src/example-common/stb_image.h"
	"src/example-common/stb_image_resize.h"
	"src/example-common/cube.hpp")
set(EXAMPLE05_SOURCE_FILES
	"src/example05-lighting/example.cpp")

add_executable(example05 WIN32 ${EXAMPLE05_HEADER_FILES} ${EXAMPLE05_SOURCE_FILES})
set_property(TARGET example05 PROPERTY CXX_STANDARD 11)
set_property(TARGET example05 PROPERTY CXX_STANDARD_REQUIRED ON)
target_link_libraries(example05 demoframework)
