#include <demoframework/framework.hpp>
#include <config.hpp>

#include <cmath>
#include <rendercore/render-storage.hpp>
#include <demoframework/filehelper.hpp>

namespace drc = rendercore;

namespace demo {

static drc::GraphicsMemory* exampleVertexMemory;
static drc::GraphicsMemory* exampleIndexMemory;
static drc::GraphicsMemory* exampleConstantsMemory;
static drc::VertexStream* exampleVertexStream;
static drc::RenderStorage* exampleStorage;
static drc::MultisampleState* exampleMultisampleState;
static drc::ViewportState* exampleViewportState;
static drc::ColorBlendState* exampleColorBlendState;
static drc::DepthStencilState* exampleDepthStencilState;
static drc::RasterizerState* exampleRasterizerState;
static drc::RenderQueue* queue;
static drc::RenderingPipeline* examplePipeline;

const char* Framework::getDemoName() const
{
	return "RenderCore Example 1 - Basics";
}

void Framework::demoInitialize()
{
	using namespace drc;

	const float vertices[] = {
		// positions
		-1.0f, -1.0f, 0.0f, 1.0f,
		1.0f, -1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 0.0f, 1.0f,
		-1.0f, 1.0f, 0.0f, 1.0f,

		// texture coordinates
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f
	};

	exampleVertexMemory = m_renderingApi->createVertexMemory(false);
	exampleVertexMemory->reallocate(sizeof(vertices));
	exampleVertexMemory->updateData(0, sizeof(vertices), vertices);

	const uint16_t indices[] = {
		0, 1, 2, 0, 2, 3
	};

	exampleIndexMemory = m_renderingApi->createIndexMemory(false);
	exampleIndexMemory->reallocate(sizeof(indices));
	exampleIndexMemory->updateData(0, sizeof(indices), indices);

	exampleConstantsMemory = m_renderingApi->createShaderConstantsMemory();
	exampleConstantsMemory->reallocate(sizeof(float) * 4);
	const float someColor[] = { 1.0f, 0.5f, 0.0f, 1.0f };
	exampleConstantsMemory->updateData(0, sizeof(float) * 4, someColor);

	VertexStreamCreateInfo vstrCreateInfo = {};
	vstrCreateInfo.indexMemoryViewInfo.mem = exampleIndexMemory;
	vstrCreateInfo.indexMemoryViewInfo.offset = 0;
	vstrCreateInfo.indexMemoryViewInfo.numIndices = 6;
	vstrCreateInfo.indexMemoryViewInfo.format = NumberFormat::UnsignedInt16;

	VertexMemoryViewInfo vmviewInfos[2] = {};
	vmviewInfos[0].mem = exampleVertexMemory;
	vmviewInfos[0].binding = 0;
	vmviewInfos[0].offset = 0;
	vmviewInfos[1].mem = exampleVertexMemory;
	vmviewInfos[1].binding = 1;
	vmviewInfos[1].offset = sizeof(float) * 16;

	vstrCreateInfo.firstVertexMemoryViewInfo = vmviewInfos;
	vstrCreateInfo.numVertexMemoryViewInfos = 2;

	exampleVertexStream = m_renderingApi->createVertexStream(vstrCreateInfo);

	ShaderCreateInfo shaderCreateInfo = {};
	shaderCreateInfo.lang = ShadingLanguage::GLSL;
	shaderCreateInfo.type = ShaderType::Fragment;
	std::string fragmentShader = readWholeFile(kAssetsDir + "shaders/example01-simple-fsh.glsl");
	shaderCreateInfo.code = (const uint8_t*)fragmentShader.c_str();
	shaderCreateInfo.codeSize = fragmentShader.length();
	Shader* fshader = m_renderingApi->createShader(shaderCreateInfo);

	shaderCreateInfo.type = ShaderType::Vertex;
	std::string vertexShader = readWholeFile(kAssetsDir + "shaders/example01-simple-vsh.glsl");

	shaderCreateInfo.code = (const uint8_t*)vertexShader.c_str();
	shaderCreateInfo.codeSize = vertexShader.length();
	Shader* vshader = m_renderingApi->createShader(shaderCreateInfo);

	RenderingPipelineCreateInfo pipelineCreateInfo = {};
	VertexInputBindingDescription ibDescs[2] = {};
	ibDescs[0].binding = 0;
	ibDescs[0].stride = sizeof(float) * 4;
	ibDescs[1].binding = 1;
	ibDescs[1].stride = sizeof(float) * 2;
	pipelineCreateInfo.viState.vertexBindingDescriptionCount = 2;
	pipelineCreateInfo.viState.vertexBindingDescriptions = ibDescs;
	VertexInputAttributeDescription iaDescs[2] = {};
	iaDescs[0].location = 0;
	iaDescs[0].binding = 0;
	iaDescs[0].numComponents = 4;
	iaDescs[0].format = drc::NumberFormat::Float;
	iaDescs[0].offset = 0;
	iaDescs[1].location = 1;
	iaDescs[1].binding = 1;
	iaDescs[1].numComponents = 2;
	iaDescs[1].format = drc::NumberFormat::Float;
	iaDescs[1].offset = 0;
	pipelineCreateInfo.viState.vertexAttributeDescriptionCount = 2;
	pipelineCreateInfo.viState.vertexAttributeDescriptions = iaDescs;
	ResourceSlotInfo vsResourceSlots[2];
	vsResourceSlots[0].objectType = ResourceSlotType::Generic;
	vsResourceSlots[0].shaderEntityName = "position";
	vsResourceSlots[1].objectType = ResourceSlotType::Generic;
	vsResourceSlots[1].shaderEntityName = "texcoords";
	ResourceSlotInfo fsResourceSlots[1];
	fsResourceSlots[0].objectType = ResourceSlotType::Generic;
	fsResourceSlots[0].shaderEntityName = "fragColor";
	pipelineCreateInfo.vertexShader.shader = vshader;
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].slotCount = 2;
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].firstResourceSlotInfo = vsResourceSlots;
	pipelineCreateInfo.fragmentShader.shader = fshader;
	pipelineCreateInfo.fragmentShader.shaderConstantsMapping.objectType = ResourceSlotType::Generic;
	pipelineCreateInfo.fragmentShader.shaderConstantsMapping.shaderEntityName = "FragmentShaderUniforms";
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].slotCount = 1;
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].firstResourceSlotInfo = fsResourceSlots;
	pipelineCreateInfo.iaState.topology = PrimitiveTopology::Triangles;
	pipelineCreateInfo.iaState.disableVertexReuse = false;
	pipelineCreateInfo.rsState.enableDepthClipping = false;
	pipelineCreateInfo.cbState.logicOp = LogicalOperation::Copy;
	pipelineCreateInfo.cbState.target[0].enableBlending = true;
	pipelineCreateInfo.cbState.target[0].channelWriteMask = 0xf; // RGBA
	examplePipeline = m_renderingApi->createRenderingPipeline(pipelineCreateInfo);

	m_renderingApi->releaseShader(vshader);
	m_renderingApi->releaseShader(fshader);

	exampleStorage = m_renderingApi->createRenderStorage();
	exampleStorage->begin();

	DepthStencilStateCreateInfo dsCreateInfo = {};
	dsCreateInfo.enableDepthTest = false;
	dsCreateInfo.enableStencilTest = false;
	dsCreateInfo.depthCompareFunc = CompareFunction::Less;
	dsCreateInfo.frontOp.stencilDepthFailOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilFailOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilPassOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilFunc = CompareFunction::Always;
	dsCreateInfo.frontOp.stencilRef = 0;
	dsCreateInfo.backOp.stencilDepthFailOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilFailOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilPassOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilFunc = CompareFunction::Always;
	dsCreateInfo.backOp.stencilRef = 0;
	exampleDepthStencilState = m_renderingApi->createDepthStencilState(dsCreateInfo);

	ViewportStateCreateInfo vpCreateInfo = {};
	vpCreateInfo.viewportCount = 1;
	vpCreateInfo.enableScissor = false;
	vpCreateInfo.viewports[0].width = static_cast<float>(m_wndWidth);
	vpCreateInfo.viewports[0].height = static_cast<float>(m_wndHeight);
	vpCreateInfo.viewports[0].minDepth = 0.0f;
	vpCreateInfo.viewports[0].maxDepth = 1.0f;
	exampleViewportState = m_renderingApi->createViewportState(vpCreateInfo);

	ColorBlendStateCreateInfo cbCreateInfo = {};
	cbCreateInfo.targets[0].enableBlending = true;
	cbCreateInfo.targets[0].srcBlendColor = BlendFactor::One;
	cbCreateInfo.targets[0].destBlendColor = BlendFactor::Zero;
	cbCreateInfo.targets[0].blendFuncColor = BlendFunction::Add;
	cbCreateInfo.targets[0].srcBlendAlpha = BlendFactor::One;
	cbCreateInfo.targets[0].destBlendAlpha = BlendFactor::Zero;
	cbCreateInfo.targets[0].blendFuncAlpha = BlendFunction::Add;
	exampleColorBlendState = m_renderingApi->createColorBlendState(cbCreateInfo);

	RenderUnit unit = {};
	unit.type = RenderUnitType::ClearOutputs;
	unit.ocm = OCM_COLOR | OCM_DEPTH;
	unit.clearColor[0] = 0.0f;
	unit.clearColor[1] = 0.0f;
	unit.clearColor[2] = 0.0f;
	unit.clearColor[3] = 1.0f;
	unit.clearDepth = 1.0f;
	unit.viewportState = exampleViewportState;
	unit.colorBlendState = exampleColorBlendState;
	unit.depthStencilState = exampleDepthStencilState;
	unit.outputTarget = m_renderingApi->getDefaultRenderOutputTarget();
	exampleStorage->store(unit);

	unit = {};
	unit.type = RenderUnitType::Draw;

	MultisampleStateCreateInfo msCreateInfo = {};
	msCreateInfo.samples = 1;
	msCreateInfo.sampleMask = 0xf;
	exampleMultisampleState = m_renderingApi->createMultisampleState(msCreateInfo);
	unit.multisampleState = exampleMultisampleState;

	unit.viewportState = exampleViewportState;

	unit.colorBlendState = exampleColorBlendState;

	unit.depthStencilState = exampleDepthStencilState;

	RasterizerStateCreateInfo rasCreateInfo = {};
	rasCreateInfo.fillMode = FillMode::Solid;
	rasCreateInfo.cullMode = CullMode::Back;
	rasCreateInfo.frontFace = FaceOrientation::CounterClockwise;
	exampleRasterizerState = m_renderingApi->createRasterizerState(rasCreateInfo);
	unit.rasterizerState = exampleRasterizerState;

	unit.pipeline = examplePipeline;
	unit.instanceCount = 1;
	unit.vstream = exampleVertexStream;
	unit.constantsMemories[4].mem = exampleConstantsMemory;
	unit.constantsMemories[4].offset = 0;
	unit.constantsMemories[4].size = static_cast<uint32_t>(exampleConstantsMemory->getCurrentSize());

	unit.outputTarget = m_renderingApi->getDefaultRenderOutputTarget();

	exampleStorage->store(unit);

	exampleStorage->end();

	queue = m_renderingApi->getRenderQueue(RenderQueueType::Universal);
}

void Framework::demoTerminate()
{
	m_renderingApi->releaseRasterizerState(exampleRasterizerState);
	m_renderingApi->releaseDepthStencilState(exampleDepthStencilState);
	m_renderingApi->releaseColorBlendState(exampleColorBlendState);
	m_renderingApi->releaseViewportState(exampleViewportState);
	m_renderingApi->releaseMultisampleState(exampleMultisampleState);
	m_renderingApi->releaseRenderStorage(exampleStorage);
	m_renderingApi->releaseRenderingPipeline(examplePipeline);
	m_renderingApi->releaseVertexStream(exampleVertexStream);
	m_renderingApi->releaseIndexMemory(exampleIndexMemory);
	m_renderingApi->releaseVertexMemory(exampleVertexMemory);
}

void Framework::demoTick(float deltaTime)
{
	static double time = 0.0;
	time += deltaTime;

	float val = static_cast<float>((std::cos(time) + 1.0) * 0.5);
	exampleConstantsMemory->updateData(sizeof(float), sizeof(float), &val);

	queue->enqueue(exampleStorage);
	queue->flush();
}

bool Framework::demoProcessSDLEvent(SDL_Event evt)
{
	if (evt.type == SDL_KEYDOWN) {
		if (evt.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
			return false;
		}
	}
	return true;
}

} // namespace demo
