#include <demoframework/framework.hpp>
#include <config.hpp>

#include <vector>
#include <functional>
#include <queue>
#include <Eigen/Dense>
#define STB_IMAGE_IMPLEMENTATION
#include "../example-common/stb_image.h"
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "../example-common/stb_image_resize.h"
#include <rendercore/render-storage.hpp>
#include "../example-common/cube.hpp"
#include <demoframework/filehelper.hpp>

namespace drc = rendercore;

namespace demo {

static drc::GraphicsMemory* exampleVertexConstantsMemory;
static drc::GraphicsMemory* exampleFragmentConstantsMemory;
static drc::RenderStorage* exampleStorage;
static drc::RenderQueue* queue;
static std::queue<std::function<void()> > cleanups;

struct VertexShaderConstants {
	alignas(16) Eigen::Matrix4f mvpMat;
};

struct FragmentShaderConstants {
	alignas(16) float someColor[4];
};

const char* Framework::getDemoName() const
{
	return "RenderCore Example 3 - 3D mesh";
}

static const float kPI = 3.14159265358979323846f;
static const float kDegToRad = kPI / 180.0f;

static Eigen::Matrix4f computeProjectionMatrix(float fov, float nearZ, float farZ, float aspect)
{
	float fovYAngle = fov * kDegToRad;
	float tangent = std::tan(fovYAngle * 0.5f);
	float invDepthSpan = 1.0f / (farZ - nearZ);
	Eigen::Matrix4f projMat;
	projMat <<
	        1.0f / (aspect * tangent), 0.0f, 0.0f, 0.0f,
	             0.0f, 1.0f / tangent, 0.0f, 0.0f,
	             0.0f, 0.0f, -(farZ + nearZ) * invDepthSpan, -(2.0f * farZ * nearZ) * invDepthSpan,
	             0.0f, 0.0f, -1.0f, 0.0f;
	return projMat;
}

static Eigen::Matrix4f computeViewMatrix(const Eigen::Quaternionf& orientation, const Eigen::Vector3f position)
{
	Eigen::Affine3f affineTransf;
	affineTransf = orientation.conjugate(); // conjugate here (norm is always == 1)
	affineTransf *= Eigen::Translation3f(-position);
	Eigen::Matrix4f viewMat;
	viewMat = affineTransf.matrix();
	return viewMat;
}

static Eigen::Matrix4f vpMat;

static void flipImageRowsInplace(uint8_t* imageData, int height, int stride)
{
	std::vector<uint8_t> tempRow(stride);
	for (int row = 0; row < (height / 2); ++row) {
		int oppositeRow = height - 1 - row;

		uint8_t* rowPtr = imageData + row * stride;
		uint8_t* oppositeRowPtr = imageData + oppositeRow * stride;

		std::memcpy(tempRow.data(), rowPtr, stride);
		std::memcpy(rowPtr, oppositeRowPtr, stride);
		std::memcpy(oppositeRowPtr, tempRow.data(), stride);
	}
}

static std::vector<std::vector<uint8_t> > generateMips(const uint8_t* imageData, int width, int height, int channels)
{
	int prevWidth = width;
	int prevHeight = height;
	int nextWidth = width / 2;
	int nextHeight = height / 2;
	std::vector<std::vector<uint8_t> > mips;
	const uint8_t* prevImageData = imageData;
	while (std::min(nextWidth, nextHeight) >= 1) {
		mips.resize(mips.size() + 1);
		mips[mips.size() - 1].resize(nextWidth * nextHeight * channels);
		stbir_resize_uint8(prevImageData, prevWidth, prevHeight, prevWidth * channels, mips[mips.size() - 1].data(), nextWidth, nextHeight, nextWidth * channels, channels);
		prevWidth = nextWidth;
		prevHeight = nextHeight;
		nextWidth /= 2;
		nextHeight /= 2;
		prevImageData = mips[mips.size() - 1].data();
	}
	return std::move(mips);
}

void Framework::demoInitialize()
{
	using namespace drc;

	GraphicsMemory* exampleVertexMemory = m_renderingApi->createVertexMemory(false);
	cleanups.push(std::bind(&RenderingAPI::releaseVertexMemory, m_renderingApi.get(), exampleVertexMemory));
	exampleVertexMemory->reallocate(sizeof(cubeVertices));
	exampleVertexMemory->updateData(0, sizeof(cubeVertices), cubeVertices);

	GraphicsMemory* exampleIndexMemory = m_renderingApi->createIndexMemory(false);
	cleanups.push(std::bind(&RenderingAPI::releaseIndexMemory, m_renderingApi.get(), exampleIndexMemory));
	exampleIndexMemory->reallocate(sizeof(cubeIndices));
	exampleIndexMemory->updateData(0, sizeof(cubeIndices), cubeIndices);

	exampleVertexConstantsMemory = m_renderingApi->createShaderConstantsMemory();
	cleanups.push(std::bind(&RenderingAPI::releaseShaderConstantsMemory, m_renderingApi.get(), exampleVertexConstantsMemory));
	exampleVertexConstantsMemory->reallocate(sizeof(VertexShaderConstants));
	VertexShaderConstants vshaderConsts = {};
	Eigen::Affine3f affine;
	affine = Eigen::AngleAxisf(45.0f * kDegToRad, Eigen::Vector3f::UnitX());
	affine *= Eigen::AngleAxisf(45.0f * kDegToRad, Eigen::Vector3f::UnitY());
	vpMat = computeProjectionMatrix(65.0f, 0.01f, 100.0f, static_cast<float>(m_wndWidth) / static_cast<float>(m_wndHeight)) * computeViewMatrix(Eigen::Quaternionf::Identity(), Eigen::Vector3f(0.0f, 0.0f, 3.0f));
	vshaderConsts.mvpMat = vpMat * affine.matrix();
	exampleVertexConstantsMemory->updateData(0, sizeof(VertexShaderConstants), &vshaderConsts);

	exampleFragmentConstantsMemory = m_renderingApi->createShaderConstantsMemory();
	cleanups.push(std::bind(&RenderingAPI::releaseShaderConstantsMemory, m_renderingApi.get(), exampleFragmentConstantsMemory));
	exampleFragmentConstantsMemory->reallocate(sizeof(FragmentShaderConstants));
	FragmentShaderConstants fshaderConsts = { { 1.0f, 1.0f, 1.0f, 1.0f } };
	exampleFragmentConstantsMemory->updateData(0, sizeof(FragmentShaderConstants), &fshaderConsts);

	VertexStreamCreateInfo vstrCreateInfo = {};
	vstrCreateInfo.indexMemoryViewInfo.mem = exampleIndexMemory;
	vstrCreateInfo.indexMemoryViewInfo.offset = 0;
	vstrCreateInfo.indexMemoryViewInfo.numIndices = sizeof(cubeIndices) / sizeof(cubeIndices[0]);
	vstrCreateInfo.indexMemoryViewInfo.format = NumberFormat::UnsignedInt16;

	VertexMemoryViewInfo vmviewInfos[1] = {};
	vmviewInfos[0].mem = exampleVertexMemory;
	vmviewInfos[0].binding = 0;
	vmviewInfos[0].offset = 0;

	vstrCreateInfo.firstVertexMemoryViewInfo = vmviewInfos;
	vstrCreateInfo.numVertexMemoryViewInfos = 1;

	VertexStream* exampleVertexStream = m_renderingApi->createVertexStream(vstrCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseVertexStream, m_renderingApi.get(), exampleVertexStream));

	// texturing
	SamplerCreateInfo sampCreateInfo = {};
	sampCreateInfo.filter = TextureFilter::MagLinearMinLinearMipLinear;
	sampCreateInfo.addressingS = TextureAddressingMode::Clamp;
	sampCreateInfo.addressingT = TextureAddressingMode::Clamp;
	sampCreateInfo.addressingR = TextureAddressingMode::Clamp;
	sampCreateInfo.mipLODBias = 0.0f;
	sampCreateInfo.maxAnisotropy = 0;
	sampCreateInfo.compareFunc = CompareFunction::Always;
	sampCreateInfo.minLOD = -1000;
	sampCreateInfo.maxLOD = 1000;
	sampCreateInfo.borderColor = BorderColor::TransparentBlack;

	Sampler* exampleSampler = m_renderingApi->createSampler(sampCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseSampler, m_renderingApi.get(), exampleSampler));

	TextureCreateInfo texCreateInfo = {};
	texCreateInfo.type = TextureType::Texture2D;
	texCreateInfo.format.numFormat = NumberFormat::sRGB;
	texCreateInfo.size.depth = 1;
	texCreateInfo.mipLevels = 1;
	texCreateInfo.arraySize = 1;
	texCreateInfo.samples = 1;
	texCreateInfo.tiling = TextureTiling::OptimalTiling;
	texCreateInfo.usage = TUSG_ShaderAccessRead;

	int imgN;
	stbi_set_flip_vertically_on_load(1);
	std::string imageFilename = kAssetsDir + "textures/Cargo0095_S.jpg";
	stbi_uc* imageData = stbi_load(imageFilename.c_str(), &texCreateInfo.size.width, &texCreateInfo.size.height, &imgN, 0);
	assert(imageData != nullptr);
	std::vector<std::vector<uint8_t> > mips = generateMips(imageData, texCreateInfo.size.width, texCreateInfo.size.height, imgN);
	texCreateInfo.mipLevels = static_cast<uint32_t>(mips.size() + 1);
	texCreateInfo.format.pxFormat =
	    imgN == 4 ? PixelFormat::RGBA :
	    imgN == 3 ? PixelFormat::RGB :
	    PixelFormat::Red;

	Texture* exampleTexture = m_renderingApi->createTexture(texCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseTexture, m_renderingApi.get(), exampleTexture));
	exampleTexture->update2D(0, 0, 0, texCreateInfo.size.width, texCreateInfo.size.height, texCreateInfo.size.width * texCreateInfo.size.height * imgN, imageData, 0);
	int mipWidth = texCreateInfo.size.width / 2;
	int mipHeight = texCreateInfo.size.height / 2;
	for (int i = 0; i < (int)mips.size(); ++i) {
		exampleTexture->update2D(i + 1, 0, 0, mipWidth, mipHeight, mips[i].size(), mips[i].data(), 0);
		mipWidth /= 2;
		mipHeight /= 2;
	}

	stbi_image_free(imageData);

	imageFilename = kAssetsDir + "textures/DecalsStain0047_1_S.png";
	imageData = stbi_load(imageFilename.c_str(), &texCreateInfo.size.width, &texCreateInfo.size.height, &imgN, 0);
	assert(imageData != nullptr);
	mips = generateMips(imageData, texCreateInfo.size.width, texCreateInfo.size.height, imgN);
	texCreateInfo.mipLevels = static_cast<uint32_t>(mips.size() + 1);
	texCreateInfo.format.pxFormat =
	    imgN == 4 ? PixelFormat::RGBA :
	    imgN == 3 ? PixelFormat::RGB :
	    PixelFormat::Red;

	Texture* exampleDecal = m_renderingApi->createTexture(texCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseTexture, m_renderingApi.get(), exampleDecal));
	exampleDecal->update2D(0, 0, 0, texCreateInfo.size.width, texCreateInfo.size.height, texCreateInfo.size.width * texCreateInfo.size.height * imgN, imageData, 0);
	mipWidth = texCreateInfo.size.width / 2;
	mipHeight = texCreateInfo.size.height / 2;
	for (int i = 0; i < (int)mips.size(); ++i) {
		exampleDecal->update2D(i + 1, 0, 0, mipWidth, mipHeight, mips[i].size(), mips[i].data(), 0);
		mipWidth /= 2;
		mipHeight /= 2;
	}

	TextureSetCreateInfo tsCreateInfo = {};
	TextureSamplerPair tsPairs[2];
	tsPairs[0].texture = exampleTexture;
	tsPairs[0].sampler = exampleSampler;
	tsPairs[1].texture = exampleDecal;
	tsPairs[1].sampler = exampleSampler;
	tsCreateInfo.firstTsPair = tsPairs;
	tsCreateInfo.numTsPairs = 2;

	TextureSet* exampleTextureSet = m_renderingApi->createTextureSet(tsCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseTextureSet, m_renderingApi.get(), exampleTextureSet));

	ShaderCreateInfo shaderCreateInfo = {};
	shaderCreateInfo.lang = ShadingLanguage::GLSL;
	shaderCreateInfo.type = ShaderType::Fragment;
	std::string fragmentShader = readWholeFile(kAssetsDir + "shaders/example03-3d-dualtex-fsh.glsl");
	shaderCreateInfo.code = (const uint8_t*)fragmentShader.c_str();
	shaderCreateInfo.codeSize = fragmentShader.length();
	Shader* fshader = m_renderingApi->createShader(shaderCreateInfo);

	shaderCreateInfo.type = ShaderType::Vertex;
	std::string vertexShader = readWholeFile(kAssetsDir + "shaders/example03-3d-dualtex-vsh.glsl");

	shaderCreateInfo.code = (const uint8_t*)vertexShader.c_str();
	shaderCreateInfo.codeSize = vertexShader.length();
	Shader* vshader = m_renderingApi->createShader(shaderCreateInfo);

	RenderingPipelineCreateInfo pipelineCreateInfo = {};
	VertexInputBindingDescription ibDescs[1] = {};
	ibDescs[0].binding = 0;
	ibDescs[0].stride = sizeof(float) * 14;
	pipelineCreateInfo.viState.vertexBindingDescriptionCount = 1;
	pipelineCreateInfo.viState.vertexBindingDescriptions = ibDescs;
	VertexInputAttributeDescription iaDescs[3] = {};
	iaDescs[0].location = 0;
	iaDescs[0].binding = 0;
	iaDescs[0].numComponents = 3;
	iaDescs[0].format = drc::NumberFormat::Float;
	iaDescs[0].offset = 0;
	iaDescs[1].location = 1;
	iaDescs[1].binding = 0;
	iaDescs[1].numComponents = 2;
	iaDescs[1].format = drc::NumberFormat::Float;
	iaDescs[1].offset = sizeof(float) * 3;
	iaDescs[2].location = 2;
	iaDescs[2].binding = 0;
	iaDescs[2].numComponents = 3;
	iaDescs[2].format = drc::NumberFormat::Float;
	iaDescs[2].offset = sizeof(float) * 5;
	pipelineCreateInfo.viState.vertexAttributeDescriptionCount = 3;
	pipelineCreateInfo.viState.vertexAttributeDescriptions = iaDescs;
	ResourceSlotInfo vsResourceSlots[3];
	vsResourceSlots[0].objectType = ResourceSlotType::Generic;
	vsResourceSlots[0].shaderEntityName = "position";
	vsResourceSlots[1].objectType = ResourceSlotType::Generic;
	vsResourceSlots[1].shaderEntityName = "texcoords";
	vsResourceSlots[2].objectType = ResourceSlotType::Generic;
	vsResourceSlots[2].shaderEntityName = "normal";
	ResourceSlotInfo fsResourceSlots[3];
	fsResourceSlots[0].objectType = ResourceSlotType::Generic;
	fsResourceSlots[0].shaderEntityName = "fragColor";
	fsResourceSlots[1].objectType = ResourceSlotType::Sampler;
	fsResourceSlots[1].shaderEntityName = "baseMap";
	fsResourceSlots[2].objectType = ResourceSlotType::Sampler;
	fsResourceSlots[2].shaderEntityName = "decalMap";
	pipelineCreateInfo.vertexShader.shader = vshader;
	pipelineCreateInfo.vertexShader.shaderConstantsMapping.objectType = ResourceSlotType::Generic;
	pipelineCreateInfo.vertexShader.shaderConstantsMapping.shaderEntityName = "VertexShaderUniforms";
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].slotCount = 3;
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].firstResourceSlotInfo = vsResourceSlots;
	pipelineCreateInfo.fragmentShader.shader = fshader;
	pipelineCreateInfo.fragmentShader.shaderConstantsMapping.objectType = ResourceSlotType::Generic;
	pipelineCreateInfo.fragmentShader.shaderConstantsMapping.shaderEntityName = "FragmentShaderUniforms";
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].slotCount = 3;
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].firstResourceSlotInfo = fsResourceSlots;
	pipelineCreateInfo.iaState.topology = PrimitiveTopology::Triangles;
	pipelineCreateInfo.iaState.disableVertexReuse = false;
	pipelineCreateInfo.rsState.enableDepthClipping = false;
	pipelineCreateInfo.cbState.logicOp = LogicalOperation::Copy;
	pipelineCreateInfo.cbState.target[0].enableBlending = true;
	pipelineCreateInfo.cbState.target[0].channelWriteMask = 0xf; // RGBA
	RenderingPipeline* examplePipeline = m_renderingApi->createRenderingPipeline(pipelineCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseRenderingPipeline, m_renderingApi.get(), examplePipeline));

	m_renderingApi->releaseShader(vshader);
	m_renderingApi->releaseShader(fshader);

	exampleStorage = m_renderingApi->createRenderStorage();
	cleanups.push(std::bind(&RenderingAPI::releaseRenderStorage, m_renderingApi.get(), exampleStorage));
	exampleStorage->begin();

	DepthStencilStateCreateInfo dsCreateInfo = {};
	dsCreateInfo.enableDepthTest = true;
	dsCreateInfo.enableDepthWrite = true;
	dsCreateInfo.depthCompareFunc = CompareFunction::Less;
	dsCreateInfo.enableDepthBounds = false;
	dsCreateInfo.enableStencilTest = false;
	dsCreateInfo.frontOp.stencilDepthFailOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilFailOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilPassOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilFunc = CompareFunction::Always;
	dsCreateInfo.frontOp.stencilRef = 0;
	dsCreateInfo.backOp.stencilDepthFailOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilFailOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilPassOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilFunc = CompareFunction::Always;
	dsCreateInfo.backOp.stencilRef = 0;
	DepthStencilState* exampleDepthStencilState = m_renderingApi->createDepthStencilState(dsCreateInfo);

	ViewportStateCreateInfo vpCreateInfo = {};
	vpCreateInfo.viewportCount = 1;
	vpCreateInfo.enableScissor = false;
	vpCreateInfo.viewports[0].width = static_cast<float>(m_wndWidth);
	vpCreateInfo.viewports[0].height = static_cast<float>(m_wndHeight);
	vpCreateInfo.viewports[0].minDepth = 0.0f;
	vpCreateInfo.viewports[0].maxDepth = 1.0f;
	ViewportState* exampleViewportState = m_renderingApi->createViewportState(vpCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseViewportState, m_renderingApi.get(), exampleViewportState));

	ColorBlendStateCreateInfo cbCreateInfo = {};
	cbCreateInfo.targets[0].enableBlending = true;
	cbCreateInfo.targets[0].srcBlendColor = BlendFactor::One;
	cbCreateInfo.targets[0].destBlendColor = BlendFactor::Zero;
	cbCreateInfo.targets[0].blendFuncColor = BlendFunction::Add;
	cbCreateInfo.targets[0].srcBlendAlpha = BlendFactor::One;
	cbCreateInfo.targets[0].destBlendAlpha = BlendFactor::Zero;
	cbCreateInfo.targets[0].blendFuncAlpha = BlendFunction::Add;
	ColorBlendState* exampleColorBlendState = m_renderingApi->createColorBlendState(cbCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseColorBlendState, m_renderingApi.get(), exampleColorBlendState));

	RenderUnit unit = {};
	unit.type = RenderUnitType::ClearOutputs;
	unit.ocm = OCM_COLOR | OCM_DEPTH;
	unit.clearColor[0] = 0.0f;
	unit.clearColor[1] = 0.0f;
	unit.clearColor[2] = 0.0f;
	unit.clearColor[3] = 1.0f;
	unit.clearDepth = 1.0f;
	unit.viewportState = exampleViewportState;
	unit.colorBlendState = exampleColorBlendState;
	unit.depthStencilState = exampleDepthStencilState;
	unit.outputTarget = m_renderingApi->getDefaultRenderOutputTarget();
	exampleStorage->store(unit);

	unit = {};
	unit.type = RenderUnitType::Draw;

	MultisampleStateCreateInfo msCreateInfo = {};
	msCreateInfo.samples = 1;
	msCreateInfo.sampleMask = 0xf;
	MultisampleState* exampleMultisampleState = m_renderingApi->createMultisampleState(msCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseMultisampleState, m_renderingApi.get(), exampleMultisampleState));
	unit.multisampleState = exampleMultisampleState;

	unit.viewportState = exampleViewportState;

	unit.colorBlendState = exampleColorBlendState;

	cleanups.push(std::bind(&RenderingAPI::releaseDepthStencilState, m_renderingApi.get(), exampleDepthStencilState));
	unit.depthStencilState = exampleDepthStencilState;

	RasterizerStateCreateInfo rasCreateInfo = {};
	rasCreateInfo.fillMode = FillMode::Solid;
	rasCreateInfo.cullMode = CullMode::Back;
	rasCreateInfo.frontFace = FaceOrientation::CounterClockwise;
	RasterizerState* exampleRasterizerState = m_renderingApi->createRasterizerState(rasCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseRasterizerState, m_renderingApi.get(), exampleRasterizerState));
	unit.rasterizerState = exampleRasterizerState;

	unit.pipeline = examplePipeline;
	unit.instanceCount = 1;
	unit.vstream = exampleVertexStream;
	unit.constantsMemories[0].mem = exampleVertexConstantsMemory;
	unit.constantsMemories[0].offset = 0;
	unit.constantsMemories[0].size = static_cast<uint32_t>(exampleVertexConstantsMemory->getCurrentSize());
	unit.constantsMemories[4].mem = exampleFragmentConstantsMemory;
	unit.constantsMemories[4].offset = 0;
	unit.constantsMemories[4].size = static_cast<uint32_t>(exampleFragmentConstantsMemory->getCurrentSize());

	unit.textureSet = exampleTextureSet;

	unit.outputTarget = m_renderingApi->getDefaultRenderOutputTarget();

	exampleStorage->store(unit);

	exampleStorage->end();

	queue = m_renderingApi->getRenderQueue(RenderQueueType::Universal);
}

void Framework::demoTerminate()
{
	while (!cleanups.empty()) {
		cleanups.front()();
		cleanups.pop();
	}

	SDL_SetRelativeMouseMode(SDL_FALSE);
}

void Framework::demoTick(float deltaTime)
{
	//static double time = 0.0;
	//time += deltaTime;

	//VertexShaderConstants vshaderConsts = {};
	//Eigen::Affine3f affine;
	//affine = Eigen::AngleAxisf(45.0f * kDegToRad, Eigen::Vector3f::UnitX());
	//affine *= Eigen::AngleAxisf((static_cast<float>(time) * 30.0f) * kDegToRad, Eigen::Vector3f::UnitY());
	//vshaderConsts.mvpMat = vpMatrix * affine.matrix();
	//exampleVertexConstantsMemory->updateData(0, sizeof(VertexShaderConstants), &vshaderConsts);

	queue->enqueue(exampleStorage);
	queue->flush();
}

bool Framework::demoProcessSDLEvent(SDL_Event evt)
{
	static bool mbDown = false;
	static Eigen::Quaternionf cubeOrientation = Eigen::Quaternionf::Identity();

	if (evt.type == SDL_KEYDOWN) {
		if (evt.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
			return false;
		}
	} else if (evt.type == SDL_MOUSEBUTTONDOWN) {
		mbDown = true;
		SDL_SetRelativeMouseMode(SDL_TRUE);
	} else if (evt.type == SDL_MOUSEBUTTONUP) {
		mbDown = false;
		SDL_SetRelativeMouseMode(SDL_FALSE);
	} else if (evt.type == SDL_MOUSEMOTION) {
		if (mbDown) {
			Eigen::Quaternionf qh, qv;
			qh = Eigen::AngleAxisf(evt.motion.xrel * kDegToRad * 0.05f, Eigen::Vector3f::UnitY());
			qv = Eigen::AngleAxisf(evt.motion.yrel * kDegToRad * 0.05f, Eigen::Vector3f::UnitX());
			cubeOrientation = qv.normalized() * qh.normalized() * cubeOrientation;

			VertexShaderConstants vshaderConsts = {};
			Eigen::Affine3f affine;
			affine = cubeOrientation;
			vshaderConsts.mvpMat = vpMat * affine.matrix();
			exampleVertexConstantsMemory->updateData(0, sizeof(VertexShaderConstants), &vshaderConsts);
		}
	}
	return true;
}

} // namespace demo
