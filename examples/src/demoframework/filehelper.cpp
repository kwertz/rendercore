#include <demoframework/filehelper.hpp>

#include <fstream>
#include <iterator>
#include <utility>

namespace demo {

	std::string readWholeFile(const std::string& filename)
	{
		std::ifstream file(filename.c_str());
		std::string text(
			std::istreambuf_iterator<char>(file),
			(std::istreambuf_iterator<char>())
		);
		return std::move(text);
	}

	std::vector<uint8_t> readWholeFileBinary(const std::string& filename)
	{
		std::ifstream file(filename.c_str(), std::ios::binary);
		file.seekg(0, std::ios::end);
		auto len = static_cast<std::vector<uint8_t>::size_type>(file.tellg());
		file.seekg(0, std::ios::beg);
		std::vector<uint8_t> fileData(static_cast<size_t>(len));
		file.read((char*)fileData.data(), static_cast<std::streamsize>(len));
		return std::move(fileData);
	}

} // namespace demo
