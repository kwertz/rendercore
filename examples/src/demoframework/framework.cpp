#include <demoframework/framework.hpp>

#include <SDL_main.h>
#ifdef WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#endif
#include <rendercore/rendering-api-factory.hpp>
#include <SDL_syswm.h>
#include <rendercore/platform-surface.hpp>

namespace demo {

Framework::Framework()
	: m_wndWidth(1280), m_wndHeight(720), m_window(), m_renderingApi(), m_swapchain(nullptr)
{}

Framework::~Framework()
{}

rendercore::RenderingAPIImplementation askAPIImplementation(SDL_Window* window)
{
	const SDL_MessageBoxButtonData buttons[] = {
		{ SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT | SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 0, "OpenGL 4.3" },
		{ 0, 1, "Vulkan" }
	};
	const SDL_MessageBoxData messageBoxData = {
		SDL_MESSAGEBOX_INFORMATION,
		window,
		"Pick your weapon",
		"Choose the rendering API to use.\nThe rendering of this example should look approximately the same regardless of API choice.",
		SDL_arraysize(buttons),
		buttons,
		NULL
	};
	int buttonID = -1;
	SDL_ShowMessageBox(&messageBoxData, &buttonID);
	switch (buttonID) {
		default:
		case 0:
			return rendercore::RenderingAPIImplementation::OpenGL4;
		case 1:
			return rendercore::RenderingAPIImplementation::Vulkan;
	}
}

void Framework::run()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Framework", "Cannot initialize SDL video", nullptr);
		return;
	}

	bool fullscreen = false;

	computeWindowSize(m_wndWidth, m_wndHeight, fullscreen);

	rendercore::RenderingAPIImplementation apiImpl = askAPIImplementation(nullptr);

	m_window.ptr = SDL_CreateWindow(
	                   getDemoName(),
	                   SDL_WINDOWPOS_CENTERED,
	                   SDL_WINDOWPOS_CENTERED,
	                   m_wndWidth,
	                   m_wndHeight,
	                   SDL_WINDOW_SHOWN | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0));

	if (m_window.ptr == nullptr) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Framework", "Cannot create a rendering window", nullptr);
		return;
	}

	SDL_SysWMinfo wmInfo;
	SDL_GetVersion(&wmInfo.version);
	SDL_GetWindowWMInfo(m_window.ptr, &wmInfo);

	rendercore::RenderingAPICreateInfo apiCreateInfo = {};
	rendercore::PlatformSurfaceReference platformSurface = {};
	apiCreateInfo.surface = &platformSurface;
	switch (wmInfo.subsystem) {
#ifdef SDL_VIDEO_DRIVER_WINDOWS
		case SDL_SYSWM_WINDOWS:
			apiCreateInfo.platformType = rendercore::Platform::Win32;
			platformSurface.win32.hinstance = ::GetModuleHandleW(NULL);
			platformSurface.win32.hwnd = wmInfo.info.win.window;
			break;
#endif
#ifdef SDL_VIDEO_DRIVER_X11
		case SDL_SYSWM_X11:
			apiCreateInfo.platformType = rendercore::Platform::Win32;
			platformSurface.xlib.display = wmInfo.info.x11.display;
			platformSurface.xlib.window = wmInfo.info.x11.window;
			break;
#endif
		default:
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Framework", "Cannot create a platform-specific drawing surface", m_window.ptr);
			break;
	}
	apiCreateInfo.appInfo.applicationName = "RenderCore example";
	apiCreateInfo.appInfo.applicationVersion = 1;
	apiCreateInfo.appInfo.engineName = "RenderCore demo framework";
	apiCreateInfo.appInfo.engineVersion = 1;
	apiCreateInfo.enableErrorChecking = true;
	apiCreateInfo.framebufferSettings.srgb = true;
	apiCreateInfo.framebufferSettings.swapchain = rendercore::SwapchainLayout::Doublebuffer;
	apiCreateInfo.framebufferSettings.numFormat = rendercore::NumberFormat::UnsignedNorm8;
	apiCreateInfo.framebufferSettings.width = static_cast<uint32_t>(m_wndWidth);
	apiCreateInfo.framebufferSettings.height = static_cast<uint32_t>(m_wndHeight);

	try {
		m_renderingApi = rendercore::RenderingAPIFactory::createRenderingAPI(apiImpl, apiCreateInfo);
	} catch (rendercore::APICreationException& exc) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Cannot create the rendering API", exc.what(), m_window.ptr);
		return;
	}

	m_swapchain = m_renderingApi->getSwapchain();

	try {
		demoInitialize();
	} catch (std::runtime_error& exc) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Run-time error", exc.what(), m_window.ptr);
		return;
	} catch (std::logic_error& exc) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Logic error", exc.what(), m_window.ptr);
		return;
	}

	Uint64 freq = SDL_GetPerformanceFrequency();
	Uint64 then = SDL_GetPerformanceCounter();

	// main loop
	bool quit = false;
	SDL_Event evt;
	while (!quit) {
		while (SDL_PollEvent(&evt)) {
			switch (evt.type) {
				case SDL_QUIT: {
					quit = true;
					break;
				}
			}
			quit |= !demoProcessSDLEvent(evt);
		}

		Uint64 now = SDL_GetPerformanceCounter();
		Uint64 deltaTime = now - then;
		then = now;
		float deltaTimeSecs = static_cast<float>(static_cast<double>(deltaTime) / static_cast<double>(freq));

		demoTick(deltaTimeSecs);
		m_swapchain->present();
	}

	demoTerminate();
}

static bool isDebugBuild()
{
#if defined(DEBUG) || defined(_DEBUG)
	return true;
#else
	return false;
#endif
}

static bool isDebuggerAttached()
{
#ifdef WIN32
	return IsDebuggerPresent() == TRUE;
#else
	return true;
#endif
}

void Framework::computeWindowSize(int& width, int& height, bool& fullscreen)
{
	SDL_DisplayMode current;

	bool debug = isDebuggerAttached();

	if (SDL_GetCurrentDisplayMode(0, &current) == 0) {
		if (!isDebugBuild() && !debug) {
			width = current.w;
			height = current.h;
			fullscreen = true;
		} else {
			width = static_cast<int>(current.w * 0.9f * 2.0f) / 2;
			height = static_cast<int>(current.h * 0.8f * 2.0f) / 2;
			fullscreen = false;
		}
	}
}

SDLWindowWrapper::SDLWindowWrapper() : ptr(nullptr) {}
SDLWindowWrapper::~SDLWindowWrapper()
{
	if (ptr != nullptr) {
		SDL_DestroyWindow(ptr);
		ptr = nullptr;
	}
}

} // namespace demo

// entry point
extern C_LINKAGE int main(int argc, char* argv[])
{
	{
		demo::Framework fw;
		fw.run();
	}

	SDL_Quit();

	return 0;
}
