#include <demoframework/framework.hpp>
#include <config.hpp>

#include <vector>
#include <functional>
#include <queue>
#include <Eigen/Dense>
#define STB_IMAGE_IMPLEMENTATION
#include "../example-common/stb_image.h"
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "../example-common/stb_image_resize.h"
#include <rendercore/render-storage.hpp>
#include "../example-common/cube.hpp"
#include <demoframework/filehelper.hpp>

namespace drc = rendercore;

namespace demo {

static drc::GraphicsMemory* exampleVertexConstantsMemory;
static drc::GraphicsMemory* exampleFragmentConstantsMemory;
static drc::RenderStorage* exampleStorage;
static drc::RenderQueue* queue;
static std::queue<std::function<void()> > cleanups;

struct VertexShaderConstants {
	alignas(16) Eigen::Matrix4f mvpMat;
	alignas(16) Eigen::Matrix4f mvMat;
	alignas(16) Eigen::Matrix4f modelMat;
	alignas(16) Eigen::Matrix4f viewMat;
	alignas(16) Eigen::Vector3f lightPos;
	alignas(16) Eigen::Vector3f camPos;
} vshaderConsts;

struct FragmentShaderConstants {
	alignas(16) Eigen::Vector3f diffuseLightColor;
	alignas(16) Eigen::Vector3f specularLightColor;
} fshaderConsts;

const char* Framework::getDemoName() const
{
	return "RenderCore Example 5 - Lighting";
}

static const float kPI = 3.14159265358979323846f;
static const float kDegToRad = kPI / 180.0f;

static Eigen::Matrix4f computeProjectionMatrix(float fov, float nearZ, float farZ, float aspect)
{
	float fovYAngle = fov * kDegToRad;
	float tangent = std::tan(fovYAngle * 0.5f);
	float invDepthSpan = 1.0f / (farZ - nearZ);
	Eigen::Matrix4f projMat;
	projMat <<
	        1.0f / (aspect * tangent), 0.0f, 0.0f, 0.0f,
	             0.0f, 1.0f / tangent, 0.0f, 0.0f,
	             0.0f, 0.0f, -(farZ + nearZ) * invDepthSpan, -(2.0f * farZ * nearZ) * invDepthSpan,
	             0.0f, 0.0f, -1.0f, 0.0f;
	return projMat;
}

static Eigen::Matrix4f computeViewMatrix(const Eigen::Quaternionf& orientation, const Eigen::Vector3f position)
{
	Eigen::Affine3f affineTransf;
	affineTransf = orientation.conjugate(); // conjugate here (norm is always == 1)
	affineTransf *= Eigen::Translation3f(-position);
	Eigen::Matrix4f viewMat;
	viewMat = affineTransf.matrix();
	return viewMat;
}

static Eigen::Matrix4f projMat;
static Eigen::Matrix4f vpMat;

static drc::VertexStream* createCube(drc::RenderingAPI* renderingApi)
{
	using namespace drc;

	GraphicsMemory* vertexMemory = renderingApi->createVertexMemory(false);
	cleanups.push(std::bind(&RenderingAPI::releaseVertexMemory, renderingApi, vertexMemory));
	vertexMemory->reallocate(sizeof(cubeVertices));
	vertexMemory->updateData(0, sizeof(cubeVertices), cubeVertices);

	GraphicsMemory* indexMemory = renderingApi->createIndexMemory(false);
	cleanups.push(std::bind(&RenderingAPI::releaseIndexMemory, renderingApi, indexMemory));
	indexMemory->reallocate(sizeof(cubeIndices));
	indexMemory->updateData(0, sizeof(cubeIndices), cubeIndices);

	VertexStreamCreateInfo vstrCreateInfo = {};
	vstrCreateInfo.indexMemoryViewInfo.mem = indexMemory;
	vstrCreateInfo.indexMemoryViewInfo.offset = 0;
	vstrCreateInfo.indexMemoryViewInfo.numIndices = sizeof(cubeIndices) / sizeof(cubeIndices[0]);
	vstrCreateInfo.indexMemoryViewInfo.format = NumberFormat::UnsignedInt16;

	VertexMemoryViewInfo vmviewInfos[1] = {};
	vmviewInfos[0].mem = vertexMemory;
	vmviewInfos[0].binding = 0;
	vmviewInfos[0].offset = 0;

	vstrCreateInfo.firstVertexMemoryViewInfo = vmviewInfos;
	vstrCreateInfo.numVertexMemoryViewInfos = 1;

	VertexStream* vertexStream = renderingApi->createVertexStream(vstrCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseVertexStream, renderingApi, vertexStream));
	return vertexStream;
}

static drc::VertexStream* createScreenAlignedQuad(drc::RenderingAPI* renderingApi)
{
	using namespace drc;

	const float vertices[] = {
		// positions
		-1.0f, -1.0f, 0.0f, 1.0f,
		1.0f, -1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 0.0f, 1.0f,
		-1.0f, 1.0f, 0.0f, 1.0f
	};

	GraphicsMemory* vertexMemory = renderingApi->createVertexMemory(false);
	cleanups.push(std::bind(&RenderingAPI::releaseVertexMemory, renderingApi, vertexMemory));
	vertexMemory->reallocate(sizeof(vertices));
	vertexMemory->updateData(0, sizeof(vertices), vertices);

	const uint16_t indices[] = {
		0, 1, 2, 0, 2, 3
	};

	GraphicsMemory* indexMemory = renderingApi->createIndexMemory(false);
	cleanups.push(std::bind(&RenderingAPI::releaseIndexMemory, renderingApi, indexMemory));
	indexMemory->reallocate(sizeof(indices));
	indexMemory->updateData(0, sizeof(indices), indices);

	VertexStreamCreateInfo vstrCreateInfo = {};
	vstrCreateInfo.indexMemoryViewInfo.mem = indexMemory;
	vstrCreateInfo.indexMemoryViewInfo.offset = 0;
	vstrCreateInfo.indexMemoryViewInfo.numIndices = sizeof(indices) / sizeof(indices[0]);
	vstrCreateInfo.indexMemoryViewInfo.format = NumberFormat::UnsignedInt16;

	VertexMemoryViewInfo vmviewInfos[1] = {};
	vmviewInfos[0].mem = vertexMemory;
	vmviewInfos[0].binding = 0;
	vmviewInfos[0].offset = 0;

	vstrCreateInfo.firstVertexMemoryViewInfo = vmviewInfos;
	vstrCreateInfo.numVertexMemoryViewInfos = 1;

	VertexStream* vertexStream = renderingApi->createVertexStream(vstrCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseVertexStream, renderingApi, vertexStream));
	return vertexStream;
}

static void createCubeShaderConstantsMemories(drc::RenderingAPI* renderingApi, int wndWidth, int wndHeight)
{
	using namespace drc;

	exampleVertexConstantsMemory = renderingApi->createShaderConstantsMemory();
	cleanups.push(std::bind(&RenderingAPI::releaseShaderConstantsMemory, renderingApi, exampleVertexConstantsMemory));
	exampleVertexConstantsMemory->reallocate(sizeof(VertexShaderConstants));
	Eigen::Affine3f affine;
	affine = Eigen::Quaternionf::Identity();
	projMat = computeProjectionMatrix(65.0f, 0.01f, 100.0f, static_cast<float>(wndWidth) / static_cast<float>(wndHeight));
	vshaderConsts.camPos = Eigen::Vector3f(0.0f, 0.0f, 3.0f);
	vshaderConsts.viewMat = computeViewMatrix(Eigen::Quaternionf::Identity(), vshaderConsts.camPos);
	vpMat = projMat * vshaderConsts.viewMat;
	vshaderConsts.modelMat = affine.matrix();
	vshaderConsts.mvpMat = vpMat * vshaderConsts.modelMat;
	vshaderConsts.mvMat = vshaderConsts.viewMat * vshaderConsts.modelMat;
	vshaderConsts.lightPos = Eigen::Vector3f(0.0f, 2.5f, 0.0f);
	exampleVertexConstantsMemory->updateData(0, sizeof(VertexShaderConstants), &vshaderConsts);

	exampleFragmentConstantsMemory = renderingApi->createShaderConstantsMemory();
	cleanups.push(std::bind(&RenderingAPI::releaseShaderConstantsMemory, renderingApi, exampleFragmentConstantsMemory));
	exampleFragmentConstantsMemory->reallocate(sizeof(FragmentShaderConstants));
	fshaderConsts.diffuseLightColor = Eigen::Vector3f(1.0f, 1.0f, 1.0f);
	fshaderConsts.specularLightColor = Eigen::Vector3f(1.0f, 1.0f, 1.0f);
	exampleFragmentConstantsMemory->updateData(0, sizeof(FragmentShaderConstants), &fshaderConsts);
}

static std::vector<std::vector<uint8_t> > generateMips(const uint8_t* imageData, int width, int height, int channels)
{
	int prevWidth = width;
	int prevHeight = height;
	int nextWidth = width / 2;
	int nextHeight = height / 2;
	std::vector<std::vector<uint8_t> > mips;
	const uint8_t* prevImageData = imageData;
	while (std::min(nextWidth, nextHeight) >= 1) {
		mips.resize(mips.size() + 1);
		mips[mips.size() - 1].resize(nextWidth * nextHeight * channels);
		stbir_resize_uint8(prevImageData, prevWidth, prevHeight, prevWidth * channels, mips[mips.size() - 1].data(), nextWidth, nextHeight, nextWidth * channels, channels);
		prevWidth = nextWidth;
		prevHeight = nextHeight;
		nextWidth /= 2;
		nextHeight /= 2;
		prevImageData = mips[mips.size() - 1].data();
	}
	return std::move(mips);
}

static drc::Texture* createTextureFromFile(drc::RenderingAPI* renderingApi, const std::string& filename, drc::TextureCreateInfo& texCreateInfo)
{
	using namespace drc;

	int imgN;
	stbi_set_flip_vertically_on_load(1);
	stbi_uc* imageData = stbi_load(filename.c_str(), &texCreateInfo.size.width, &texCreateInfo.size.height, &imgN, 0);
	assert(imageData != nullptr);
	std::vector<std::vector<uint8_t> > mips = generateMips(imageData, texCreateInfo.size.width, texCreateInfo.size.height, imgN);
	texCreateInfo.mipLevels = static_cast<uint32_t>(mips.size() + 1);
	texCreateInfo.format.pxFormat =
	    imgN == 4 ? PixelFormat::RGBA :
	    imgN == 3 ? PixelFormat::RGB :
	    PixelFormat::Red;

	Texture* texture = renderingApi->createTexture(texCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseTexture, renderingApi, texture));
	texture->update2D(0, 0, 0, texCreateInfo.size.width, texCreateInfo.size.height, texCreateInfo.size.width * texCreateInfo.size.height * imgN, imageData, 0);
	int mipWidth = texCreateInfo.size.width / 2;
	int mipHeight = texCreateInfo.size.height / 2;
	for (int i = 0; i < (int)mips.size(); ++i) {
		texture->update2D(i + 1, 0, 0, mipWidth, mipHeight, mips[i].size(), mips[i].data(), 0);
		mipWidth /= 2;
		mipHeight /= 2;
	}

	stbi_image_free(imageData);

	return texture;
}

static drc::TextureSet* createCubeTextureSet(drc::RenderingAPI* renderingApi)
{
	using namespace drc;

	SamplerCreateInfo sampCreateInfo = {};
	sampCreateInfo.filter = TextureFilter::MagLinearMinLinearMipLinear;
	sampCreateInfo.addressingS = TextureAddressingMode::Clamp;
	sampCreateInfo.addressingT = TextureAddressingMode::Clamp;
	sampCreateInfo.addressingR = TextureAddressingMode::Clamp;
	sampCreateInfo.mipLODBias = 0.0f;
	sampCreateInfo.maxAnisotropy = 0;
	sampCreateInfo.compareFunc = CompareFunction::Always;
	sampCreateInfo.minLOD = -1000;
	sampCreateInfo.maxLOD = 1000;
	sampCreateInfo.borderColor = BorderColor::TransparentBlack;

	Sampler* sampler = renderingApi->createSampler(sampCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseSampler, renderingApi, sampler));

	TextureCreateInfo texCreateInfo = {};
	texCreateInfo.type = TextureType::Texture2D;
	texCreateInfo.size.depth = 1;
	texCreateInfo.mipLevels = 1;
	texCreateInfo.arraySize = 1;
	texCreateInfo.samples = 1;
	texCreateInfo.tiling = TextureTiling::OptimalTiling;
	texCreateInfo.usage = TUSG_ShaderAccessRead;

	texCreateInfo.format.numFormat = NumberFormat::sRGB;
	Texture* baseTexture = createTextureFromFile(renderingApi, kAssetsDir + "textures/Floor_Tile_01_D.png", texCreateInfo);
	texCreateInfo.format.numFormat = NumberFormat::UnsignedNorm8;
	Texture* normalTexture = createTextureFromFile(renderingApi, kAssetsDir + "textures/Floor_Tile_01_N.png", texCreateInfo);
	Texture* specTexture = createTextureFromFile(renderingApi, kAssetsDir + "textures/default-specular.png", texCreateInfo);

	TextureSetCreateInfo tsCreateInfo = {};
	TextureSamplerPair tsPairs[3];
	tsPairs[0].texture = baseTexture;
	tsPairs[0].sampler = sampler;
	tsPairs[1].texture = normalTexture;
	tsPairs[1].sampler = sampler;
	tsPairs[2].texture = specTexture;
	tsPairs[2].sampler = sampler;
	tsCreateInfo.firstTsPair = tsPairs;
	tsCreateInfo.numTsPairs = 3;

	TextureSet* textureSet = renderingApi->createTextureSet(tsCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseTextureSet, renderingApi, textureSet));

	return textureSet;
}

static drc::RenderingPipeline* createCubeRenderingPipeline(drc::RenderingAPI* renderingApi)
{
	using namespace drc;

	ShaderCreateInfo shaderCreateInfo = {};
	shaderCreateInfo.lang = ShadingLanguage::GLSL;
	shaderCreateInfo.type = ShaderType::Fragment;
	std::string fragmentShader = readWholeFile(kAssetsDir + "shaders/example05-lighted-cube-two-lights-fsh.glsl");
	shaderCreateInfo.code = (const uint8_t*)fragmentShader.c_str();
	shaderCreateInfo.codeSize = fragmentShader.length();
	Shader* fshader = renderingApi->createShader(shaderCreateInfo);

	shaderCreateInfo.type = ShaderType::Vertex;
	std::string vertexShader = readWholeFile(kAssetsDir + "shaders/example05-lighted-cube-two-lights-vsh.glsl");

	shaderCreateInfo.code = (const uint8_t*)vertexShader.c_str();
	shaderCreateInfo.codeSize = vertexShader.length();
	Shader* vshader = renderingApi->createShader(shaderCreateInfo);

	RenderingPipelineCreateInfo pipelineCreateInfo = {};
	VertexInputBindingDescription ibDescs[1] = {};
	ibDescs[0].binding = 0;
	ibDescs[0].stride = sizeof(float) * 14;
	pipelineCreateInfo.viState.vertexBindingDescriptionCount = 1;
	pipelineCreateInfo.viState.vertexBindingDescriptions = ibDescs;
	VertexInputAttributeDescription iaDescs[5] = {};
	iaDescs[0].location = 0;
	iaDescs[0].binding = 0;
	iaDescs[0].numComponents = 3;
	iaDescs[0].format = drc::NumberFormat::Float;
	iaDescs[0].offset = 0;
	iaDescs[1].location = 1;
	iaDescs[1].binding = 0;
	iaDescs[1].numComponents = 2;
	iaDescs[1].format = drc::NumberFormat::Float;
	iaDescs[1].offset = sizeof(float) * 3;
	iaDescs[2].location = 2;
	iaDescs[2].binding = 0;
	iaDescs[2].numComponents = 3;
	iaDescs[2].format = drc::NumberFormat::Float;
	iaDescs[2].offset = sizeof(float) * 5;
	iaDescs[3].location = 3;
	iaDescs[3].binding = 0;
	iaDescs[3].numComponents = 3;
	iaDescs[3].format = drc::NumberFormat::Float;
	iaDescs[3].offset = sizeof(float) * 8;
	iaDescs[4].location = 4;
	iaDescs[4].binding = 0;
	iaDescs[4].numComponents = 3;
	iaDescs[4].format = drc::NumberFormat::Float;
	iaDescs[4].offset = sizeof(float) * 11;
	pipelineCreateInfo.viState.vertexAttributeDescriptionCount = 5;
	pipelineCreateInfo.viState.vertexAttributeDescriptions = iaDescs;
	ResourceSlotInfo vsResourceSlots[5];
	vsResourceSlots[0].objectType = ResourceSlotType::Generic;
	vsResourceSlots[0].shaderEntityName = "position";
	vsResourceSlots[1].objectType = ResourceSlotType::Generic;
	vsResourceSlots[1].shaderEntityName = "texcoords";
	vsResourceSlots[2].objectType = ResourceSlotType::Generic;
	vsResourceSlots[2].shaderEntityName = "normal";
	vsResourceSlots[3].objectType = ResourceSlotType::Generic;
	vsResourceSlots[3].shaderEntityName = "tangent";
	vsResourceSlots[4].objectType = ResourceSlotType::Generic;
	vsResourceSlots[4].shaderEntityName = "bitangent";
	ResourceSlotInfo fsResourceSlots[4];
	fsResourceSlots[0].objectType = ResourceSlotType::Generic;
	fsResourceSlots[0].shaderEntityName = "outFragColor";
	fsResourceSlots[1].objectType = ResourceSlotType::Sampler;
	fsResourceSlots[1].shaderEntityName = "baseMap";
	fsResourceSlots[2].objectType = ResourceSlotType::Sampler;
	fsResourceSlots[2].shaderEntityName = "normalMap";
	fsResourceSlots[3].objectType = ResourceSlotType::Sampler;
	fsResourceSlots[3].shaderEntityName = "specMap";
	pipelineCreateInfo.vertexShader.shader = vshader;
	pipelineCreateInfo.vertexShader.shaderConstantsMapping.objectType = ResourceSlotType::Generic;
	pipelineCreateInfo.vertexShader.shaderConstantsMapping.shaderEntityName = "VertexShaderUniforms";
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].slotCount = 5;
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].firstResourceSlotInfo = vsResourceSlots;
	pipelineCreateInfo.fragmentShader.shader = fshader;
	pipelineCreateInfo.fragmentShader.shaderConstantsMapping.objectType = ResourceSlotType::Generic;
	pipelineCreateInfo.fragmentShader.shaderConstantsMapping.shaderEntityName = "FragmentShaderUniforms";
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].slotCount = 4;
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].firstResourceSlotInfo = fsResourceSlots;
	pipelineCreateInfo.iaState.topology = PrimitiveTopology::Triangles;
	pipelineCreateInfo.iaState.disableVertexReuse = false;
	pipelineCreateInfo.rsState.enableDepthClipping = false;
	pipelineCreateInfo.cbState.logicOp = LogicalOperation::Copy;
	pipelineCreateInfo.cbState.target[0].enableBlending = true;
	pipelineCreateInfo.cbState.target[0].channelWriteMask = 0xf; // RGBA
	RenderingPipeline* pipeline = renderingApi->createRenderingPipeline(pipelineCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseRenderingPipeline, renderingApi, pipeline));

	renderingApi->releaseShader(vshader);
	renderingApi->releaseShader(fshader);

	return pipeline;
}

static drc::RenderingPipeline* createPostEffectRenderingPipeline(drc::RenderingAPI* renderingApi)
{
	using namespace drc;

	ShaderCreateInfo shaderCreateInfo = {};
	shaderCreateInfo.lang = ShadingLanguage::GLSL;
	shaderCreateInfo.type = ShaderType::Fragment;
	std::string fragmentShader = readWholeFile(kAssetsDir + "shaders/example05-passthrough-fsh.glsl");
	shaderCreateInfo.code = (const uint8_t*)fragmentShader.c_str();
	shaderCreateInfo.codeSize = fragmentShader.length();
	Shader* fshader = renderingApi->createShader(shaderCreateInfo);

	shaderCreateInfo.type = ShaderType::Vertex;
	std::string vertexShader = readWholeFile(kAssetsDir + "shaders/example04-postfx-quad-vsh.glsl");

	shaderCreateInfo.code = (const uint8_t*)vertexShader.c_str();
	shaderCreateInfo.codeSize = vertexShader.length();
	Shader* vshader = renderingApi->createShader(shaderCreateInfo);

	RenderingPipelineCreateInfo pipelineCreateInfo = {};
	VertexInputBindingDescription ibDescs[1] = {};
	ibDescs[0].binding = 0;
	ibDescs[0].stride = sizeof(float) * 4;
	pipelineCreateInfo.viState.vertexBindingDescriptionCount = 1;
	pipelineCreateInfo.viState.vertexBindingDescriptions = ibDescs;
	VertexInputAttributeDescription iaDescs[1] = {};
	iaDescs[0].location = 0;
	iaDescs[0].binding = 0;
	iaDescs[0].numComponents = 4;
	iaDescs[0].format = drc::NumberFormat::Float;
	iaDescs[0].offset = 0;
	pipelineCreateInfo.viState.vertexAttributeDescriptionCount = 1;
	pipelineCreateInfo.viState.vertexAttributeDescriptions = iaDescs;
	ResourceSlotInfo vsResourceSlots[1];
	vsResourceSlots[0].objectType = ResourceSlotType::Generic;
	vsResourceSlots[0].shaderEntityName = "position";
	ResourceSlotInfo fsResourceSlots[2];
	fsResourceSlots[0].objectType = ResourceSlotType::Generic;
	fsResourceSlots[0].shaderEntityName = "fragColor";
	fsResourceSlots[1].objectType = ResourceSlotType::Sampler;
	fsResourceSlots[1].shaderEntityName = "baseMap";
	pipelineCreateInfo.vertexShader.shader = vshader;
	pipelineCreateInfo.vertexShader.shaderConstantsMapping.objectType = ResourceSlotType::Unused;
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].slotCount = 1;
	pipelineCreateInfo.vertexShader.resourceSetMapping[0].firstResourceSlotInfo = vsResourceSlots;
	pipelineCreateInfo.fragmentShader.shader = fshader;
	pipelineCreateInfo.fragmentShader.shaderConstantsMapping.objectType = ResourceSlotType::Unused;
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].slotCount = 2;
	pipelineCreateInfo.fragmentShader.resourceSetMapping[0].firstResourceSlotInfo = fsResourceSlots;
	pipelineCreateInfo.iaState.topology = PrimitiveTopology::Triangles;
	pipelineCreateInfo.iaState.disableVertexReuse = false;
	pipelineCreateInfo.rsState.enableDepthClipping = false;
	pipelineCreateInfo.cbState.logicOp = LogicalOperation::Copy;
	pipelineCreateInfo.cbState.target[0].enableBlending = true;
	pipelineCreateInfo.cbState.target[0].channelWriteMask = 0xf; // RGBA
	RenderingPipeline* pipeline = renderingApi->createRenderingPipeline(pipelineCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseRenderingPipeline, renderingApi, pipeline));

	renderingApi->releaseShader(vshader);
	renderingApi->releaseShader(fshader);

	return pipeline;
}

static drc::RenderOutputTarget* createOutputTarget(drc::RenderingAPI* renderingApi, drc::Texture*& tgtTex, int width, int height)
{
	using namespace drc;

	RenderOutputBufferCreateInfo robCreateInfo = {};
	robCreateInfo.format.numFormat = NumberFormat::DepthStencil;
	robCreateInfo.format.pxFormat = PixelFormat::DepthStencil;
	robCreateInfo.size.width = width;
	robCreateInfo.size.height = height;
	RenderOutputBuffer* rob = renderingApi->createRenderOutputBuffer(robCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseRenderOutputBuffer, renderingApi, rob));

	TextureCreateInfo tgtTexCreateInfo = {};
	tgtTexCreateInfo.type = TextureType::Texture2D;
	tgtTexCreateInfo.format.numFormat = NumberFormat::UnsignedNorm16;
	tgtTexCreateInfo.format.pxFormat = PixelFormat::RGB;
	tgtTexCreateInfo.size.width = width;
	tgtTexCreateInfo.size.height = height;
	tgtTexCreateInfo.size.depth = 1;
	tgtTexCreateInfo.mipLevels = 1;
	tgtTexCreateInfo.arraySize = 1;
	tgtTexCreateInfo.samples = 1;
	tgtTexCreateInfo.tiling = TextureTiling::OptimalTiling;
	tgtTexCreateInfo.usage = TUSG_ColorTarget;
	tgtTex = renderingApi->createTexture(tgtTexCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseTexture, renderingApi, tgtTex));

	RenderOutputTargetCreateInfo otgtCreateInfo = {};
	RenderOutputBufferAttachmentInfo robAttachment = {};
	robAttachment.type = RenderOutputAttachmentType::DepthStencilAttachment;
	robAttachment.buffer = rob;
	TextureAttachmentInfo texAttachment = {};
	texAttachment.type = RenderOutputAttachmentType::ColorAttachment;
	texAttachment.texture = tgtTex;
	texAttachment.colorAttachmentIndex = 0;
	texAttachment.mipLevel = 0;
	otgtCreateInfo.firstBufferAttachmentInfo = &robAttachment;
	otgtCreateInfo.numBufferAttachmentInfos = 1;
	otgtCreateInfo.firstTextureAttachmentInfo = &texAttachment;
	otgtCreateInfo.numTextureAttachmentInfos = 1;
	RenderOutputTarget* otgt = renderingApi->createRenderOutputTarget(otgtCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseRenderOutputTarget, renderingApi, otgt));

	return otgt;
}

static void createStateObjects(drc::RenderingAPI* renderingApi, drc::RenderUnit& unit, drc::DepthStencilState*& outputDepthStencilState, int wndWidth, int wndHeight)
{
	using namespace drc;

	MultisampleStateCreateInfo msCreateInfo = {};
	msCreateInfo.samples = 1;
	msCreateInfo.sampleMask = 0xf;
	MultisampleState* exampleMultisampleState = renderingApi->createMultisampleState(msCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseMultisampleState, renderingApi, exampleMultisampleState));
	unit.multisampleState = exampleMultisampleState;

	ViewportStateCreateInfo vpCreateInfo = {};
	vpCreateInfo.viewportCount = 1;
	vpCreateInfo.enableScissor = false;
	vpCreateInfo.viewports[0].width = static_cast<float>(wndWidth);
	vpCreateInfo.viewports[0].height = static_cast<float>(wndHeight);
	vpCreateInfo.viewports[0].minDepth = 0.0f;
	vpCreateInfo.viewports[0].maxDepth = 1.0f;
	ViewportState* exampleViewportState = renderingApi->createViewportState(vpCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseViewportState, renderingApi, exampleViewportState));
	unit.viewportState = exampleViewportState;

	ColorBlendStateCreateInfo cbCreateInfo = {};
	cbCreateInfo.targets[0].enableBlending = true;
	cbCreateInfo.targets[0].srcBlendColor = BlendFactor::One;
	cbCreateInfo.targets[0].destBlendColor = BlendFactor::Zero;
	cbCreateInfo.targets[0].blendFuncColor = BlendFunction::Add;
	cbCreateInfo.targets[0].srcBlendAlpha = BlendFactor::One;
	cbCreateInfo.targets[0].destBlendAlpha = BlendFactor::Zero;
	cbCreateInfo.targets[0].blendFuncAlpha = BlendFunction::Add;
	ColorBlendState* exampleColorBlendState = renderingApi->createColorBlendState(cbCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseColorBlendState, renderingApi, exampleColorBlendState));
	unit.colorBlendState = exampleColorBlendState;

	DepthStencilStateCreateInfo dsCreateInfo = {};
	dsCreateInfo.enableDepthTest = true;
	dsCreateInfo.enableDepthWrite = true;
	dsCreateInfo.depthCompareFunc = CompareFunction::Less;
	dsCreateInfo.enableDepthBounds = false;
	dsCreateInfo.enableStencilTest = false;
	dsCreateInfo.frontOp.stencilDepthFailOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilFailOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilPassOp = StencilOperation::Keep;
	dsCreateInfo.frontOp.stencilFunc = CompareFunction::Always;
	dsCreateInfo.frontOp.stencilRef = 0;
	dsCreateInfo.backOp.stencilDepthFailOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilFailOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilPassOp = StencilOperation::Keep;
	dsCreateInfo.backOp.stencilFunc = CompareFunction::Always;
	dsCreateInfo.backOp.stencilRef = 0;
	DepthStencilState* exampleDepthStencilState = renderingApi->createDepthStencilState(dsCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseDepthStencilState, renderingApi, exampleDepthStencilState));
	unit.depthStencilState = exampleDepthStencilState;

	// for the screen-aligned quad
	dsCreateInfo.enableDepthTest = false;
	dsCreateInfo.enableDepthWrite = false;
	outputDepthStencilState = renderingApi->createDepthStencilState(dsCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseDepthStencilState, renderingApi, outputDepthStencilState));

	RasterizerStateCreateInfo rasCreateInfo = {};
	rasCreateInfo.fillMode = FillMode::Solid;
	rasCreateInfo.cullMode = CullMode::Back;
	rasCreateInfo.frontFace = FaceOrientation::CounterClockwise;
	RasterizerState* exampleRasterizerState = renderingApi->createRasterizerState(rasCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseRasterizerState, renderingApi, exampleRasterizerState));
	unit.rasterizerState = exampleRasterizerState;
}

void Framework::demoInitialize()
{
	using namespace drc;

	VertexStream* exampleVertexStream = createCube(m_renderingApi.get());
	VertexStream* saqVertexStream = createScreenAlignedQuad(m_renderingApi.get());

	createCubeShaderConstantsMemories(m_renderingApi.get(), m_wndWidth, m_wndHeight);

	TextureSet* exampleTextureSet = createCubeTextureSet(m_renderingApi.get());

	RenderingPipeline* examplePipeline = createCubeRenderingPipeline(m_renderingApi.get());
	RenderingPipeline* outputPipeline = createPostEffectRenderingPipeline(m_renderingApi.get());

	Texture* tgtTex;
	RenderOutputTarget* otgt = createOutputTarget(m_renderingApi.get(), tgtTex, m_wndWidth, m_wndHeight);

	SamplerCreateInfo psampCreateInfo = {};
	psampCreateInfo.filter = TextureFilter::MagNearestMinNearestMipNearest;
	psampCreateInfo.addressingS = TextureAddressingMode::Clamp;
	psampCreateInfo.addressingT = TextureAddressingMode::Clamp;
	psampCreateInfo.addressingR = TextureAddressingMode::Clamp;
	psampCreateInfo.mipLODBias = 0.0f;
	psampCreateInfo.maxAnisotropy = 0;
	psampCreateInfo.compareFunc = CompareFunction::Always;
	psampCreateInfo.minLOD = -1000;
	psampCreateInfo.maxLOD = 1000;
	psampCreateInfo.borderColor = BorderColor::TransparentBlack;

	Sampler* plainSampler = m_renderingApi->createSampler(psampCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseSampler, m_renderingApi.get(), plainSampler));

	exampleStorage = m_renderingApi->createRenderStorage();
	cleanups.push(std::bind(&RenderingAPI::releaseRenderStorage, m_renderingApi.get(), exampleStorage));
	exampleStorage->begin();

	RenderUnit drawUnit = {};
	drawUnit.type = RenderUnitType::Draw;

	DepthStencilState* outputDepthStencilState;
	createStateObjects(m_renderingApi.get(), drawUnit, outputDepthStencilState, m_wndWidth, m_wndHeight);

	// clear the scene target
	RenderUnit clearUnit = {};
	clearUnit.type = RenderUnitType::ClearOutputs;
	clearUnit.ocm = OCM_COLOR | OCM_DEPTH;
	clearUnit.clearColor[0] = 0.0f;
	clearUnit.clearColor[1] = 0.0f;
	clearUnit.clearColor[2] = 0.0f;
	clearUnit.clearColor[3] = 1.0f;
	clearUnit.clearDepth = 1.0f;

	clearUnit.viewportState = drawUnit.viewportState;
	clearUnit.colorBlendState = drawUnit.colorBlendState;
	clearUnit.depthStencilState = drawUnit.depthStencilState;
	clearUnit.outputTarget = otgt;
	exampleStorage->store(clearUnit);

	drawUnit.pipeline = examplePipeline;
	drawUnit.instanceCount = 3;
	drawUnit.vstream = exampleVertexStream;
	drawUnit.constantsMemories[0].mem = exampleVertexConstantsMemory;
	drawUnit.constantsMemories[0].offset = 0;
	drawUnit.constantsMemories[0].size = static_cast<uint32_t>(exampleVertexConstantsMemory->getCurrentSize());
	drawUnit.constantsMemories[4].mem = exampleFragmentConstantsMemory;
	drawUnit.constantsMemories[4].offset = 0;
	drawUnit.constantsMemories[4].size = static_cast<uint32_t>(exampleFragmentConstantsMemory->getCurrentSize());

	drawUnit.textureSet = exampleTextureSet;

	drawUnit.outputTarget = otgt;
	//unit.outputTarget = m_renderingApi->getDefaultRenderOutputTarget();

	exampleStorage->store(drawUnit);

	// clear the default target
	clearUnit.depthStencilState = outputDepthStencilState;
	clearUnit.outputTarget = m_renderingApi->getDefaultRenderOutputTarget();
	exampleStorage->store(clearUnit);

	// output to default target
	drawUnit.depthStencilState = outputDepthStencilState;
	drawUnit.pipeline = outputPipeline;
	drawUnit.vstream = saqVertexStream;
	drawUnit.constantsMemories[0].mem = nullptr;
	drawUnit.constantsMemories[4].mem = nullptr;

	TextureSetCreateInfo outputTsCreateInfo = {};
	TextureSamplerPair outputTsPair = {};
	outputTsPair.sampler = plainSampler;
	outputTsPair.texture = tgtTex;
	outputTsCreateInfo.firstTsPair = &outputTsPair;
	outputTsCreateInfo.numTsPairs = 1;

	TextureSet* outputTs = m_renderingApi->createTextureSet(outputTsCreateInfo);
	cleanups.push(std::bind(&RenderingAPI::releaseTextureSet, m_renderingApi.get(), outputTs));

	drawUnit.textureSet = outputTs;

	drawUnit.outputTarget = m_renderingApi->getDefaultRenderOutputTarget();

	exampleStorage->store(drawUnit);

	exampleStorage->end();

	queue = m_renderingApi->getRenderQueue(RenderQueueType::Universal);
}

void Framework::demoTerminate()
{
	while (!cleanups.empty()) {
		cleanups.front()();
		cleanups.pop();
	}
}

void Framework::demoTick(float deltaTime)
{
	queue->enqueue(exampleStorage);
	queue->flush();
}

static void recomputeModelMatrix(const Eigen::Vector3f& translation, const Eigen::Quaternionf& orientation)
{
	Eigen::Affine3f affine;
	affine = Eigen::Translation3f(translation);
	affine *= orientation;
	vshaderConsts.modelMat = affine.matrix();
	vshaderConsts.mvpMat = vpMat * vshaderConsts.modelMat;
	vshaderConsts.mvMat = vshaderConsts.viewMat * vshaderConsts.modelMat;
	exampleVertexConstantsMemory->updateData(0, sizeof(VertexShaderConstants), &vshaderConsts);
}

bool Framework::demoProcessSDLEvent(SDL_Event evt)
{
	static bool mbDown = false;
	static Eigen::Quaternionf cubeOrientation = Eigen::Quaternionf::Identity();
	static float cubeDist = 0.0f;

	if (evt.type == SDL_KEYDOWN) {
		if (evt.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
			return false;
		}
	} else if (evt.type == SDL_MOUSEBUTTONDOWN) {
		mbDown = true;
		SDL_SetRelativeMouseMode(SDL_TRUE);
	} else if (evt.type == SDL_MOUSEBUTTONUP) {
		mbDown = false;
		SDL_SetRelativeMouseMode(SDL_FALSE);
	} else if (evt.type == SDL_MOUSEMOTION) {
		if (mbDown) {
			Eigen::Quaternionf qh, qv;
			qh = Eigen::AngleAxisf(evt.motion.xrel * kDegToRad * 0.05f, Eigen::Vector3f::UnitY());
			qv = Eigen::AngleAxisf(evt.motion.yrel * kDegToRad * 0.05f, Eigen::Vector3f::UnitX());
			cubeOrientation = qv.normalized() * qh.normalized() * cubeOrientation;
			recomputeModelMatrix(Eigen::Vector3f(0.0f, 0.0f, cubeDist), cubeOrientation);
		}
	} else if (evt.type == SDL_MOUSEWHEEL) {
		cubeDist += evt.wheel.y * -0.5f;
		recomputeModelMatrix(Eigen::Vector3f(0.0f, 0.0f, cubeDist), cubeOrientation);
	}
	return true;
}

} // namespace demo
